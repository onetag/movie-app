import React, { Component } from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const Header = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Moovi</Text>
    </View>
  )
}

export default Header;
