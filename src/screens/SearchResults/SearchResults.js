import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
// import Header from '../../components/Header';
// import Footer from '../../components/Footer';
import SearchResults from '../../components/SearchResults';

const SearchResultsScreen = () => {
  return (
    <View style={styles.container}>
      <SearchResults />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F8F8F8'
  }
})

export default SearchResultsScreen;
