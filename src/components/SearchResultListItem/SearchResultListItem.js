import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import SeeMoreIcon from './assets/see-more.png';

const typeMap = (type) => {
  switch (type) {
    case 'movie':
      return 'Фильм';
    case 'series':
      return 'Сериал';
    default:
      return type;
  }
}

const SearchResultListItem = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.movieImageContainer}>
        <Image style={ styles.movieImage } source={{ uri: props.image }} />
      </View>
      <View style={styles.descriptionContainer}>
        <TouchableOpacity onPress={Actions.movie} style={styles.seeMoreButton}>
          <Image style={styles.seeMoreIcon} source={SeeMoreIcon} />
        </TouchableOpacity>
        <View style={styles.description}>
          <Text
            numberOfLines={1}
            ellipsizeMode='tail'
            style={styles.title}
          >
            {props.title}
          </Text>
          <Text style={styles.year}>{props.year}</Text>
          <Text style={styles.type}>{typeMap(props.type)}</Text>
        </View>
      </View>
    </View>
  )
}

export default SearchResultListItem;
