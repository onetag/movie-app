import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 64,
    backgroundColor: '#0CBAFF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 20,
    marginTop: 20,
    color: 'white',
    fontWeight: 'bold'
  }
})

export default styles;
