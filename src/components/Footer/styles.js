import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 64,
    backgroundColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 20
  }
})

export default styles;
