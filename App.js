import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Router, Stack, Scene } from 'react-native-router-flux';
import SearchResults from './src/screens/SearchResults';
import MovieDetails from './src/screens/MovieDetails';

export default class App extends Component {
  render() {
    return (
      <Router>
        <Stack key="root">
          <Scene
            title="Moovi"
            key="SearchResults"
            component={SearchResults}
            navigationBarStyle={styles.headerBarStyle}
            titleStyle={styles.headerTitleStyle}
          />
          <Scene
            title="Detail Info"
            key="movie"
            component={MovieDetails}
            navigationBarStyle={styles.headerBarStyle}
            titleStyle={styles.headerTitleStyle}
          />
        </Stack>
      </Router>
    );
  }
}

const styles = StyleSheet.create({
  headerBarStyle: {
    backgroundColor: '#0CBAFF',
    borderBottomWidth: 0
  },
  headerTitleStyle: {
    color: '#fff',
    fontSize: 17
  }
})
