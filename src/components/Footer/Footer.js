import React, { Component } from 'react';
import { View, Text } from 'react-native';
import styles from './styles';

const Footer = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Footer</Text>
    </View>
  )
}

export default Footer;
