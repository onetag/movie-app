import React, { Component } from 'react';
import { ScrollView, Text } from 'react-native';
import styles from './styles';
import axios from 'axios';
import _ from 'lodash';
import SearchResultListItem from '../SearchResultListItem'

class SearchResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      results: []
    }
  }

  processMovieData(data) {
    return {
      id: data.imdbID,
      type: data.Type,
      title: data.Title,
      year: data.Year,
      image: data.Poster
    }
  }

  renderResults() {
    return _.map(
      this.state.results,
      result => <SearchResultListItem key={result.imdbID} {...this.processMovieData(result)} />
    )
  }

  componentWillMount() {
    axios
      .get('http://www.omdbapi.com/?apikey=725d2545&s=Spider&page=1')
      .then(result => this.setState({ results: result.data.Search }))
  }

  render() {
    return (
      <ScrollView style={styles.container}>
      {this.renderResults()}
      </ScrollView>
    )
  }
}

export default SearchResults;
