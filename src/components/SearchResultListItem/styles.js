import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 120,
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: 'row'
  },
  movieImageContainer: {
    width: 80,
    height: 100,
    zIndex: 10,
    shadowColor: '#000',
    shadowOpacity: 0.3,
    shadowOffset: {
      width: 10,
      height: 2
    },
    shadowRadius: 5
  },
  movieImage: {
    width: 100,
    height: 100,
    borderRadius: 8,
    position: 'absolute'
  },
  descriptionContainer: {
    flex: 2
  },
  description: {
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: 100,
    marginTop: 20,
    marginRight: 20,
    shadowColor: '#000',
    shadowOpacity: 0.07,
    shadowOffset: {
      width: 0,
      height: 3
    }
  },
  title: {
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 35,
    marginRight: 10,
    marginTop: 12
  },
  year: {
    fontSize: 13,
    marginLeft: 35,
    marginTop: 5,
    color: '#959595'
  },
  type: {
    fontSize: 14,
    marginLeft: 35,
    marginTop: 18,
    color: '#000'
  },
  seeMoreButton: {
    position: 'absolute',
    backgroundColor: '#0CBAFF',
    width: 70,
    height: 46,
    justifyContent: 'center',
    borderRadius: 8,
    alignSelf: 'flex-end',
    marginTop: 54,
    marginRight: -20,
    zIndex: 10,
    shadowColor: '#0CBAFF',
    shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 3
    }
  },
  seeMoreIcon: {
    width: 39,
    height: 23,
    alignSelf: 'center'
  }
})

export default styles;
